FactoryGirl.define do
  factory :bread do
    name "MyString"
    weight 1
    length 1
    image "MyString"
    price 1
  end
end
