require 'rails'
require 'adminos'
require 'rails/test_help'
require 'rails/generators/test_case'

DUMMY_PATH        = File.expand_path '../dummy', __FILE__
DUMMY_LOCALE_PATH = File.expand_path '../dummy_with_locale', __FILE__

unless File.directory? DUMMY_PATH
  system %Q(rails _3.2.13_ new #{DUMMY_PATH} -d postgresql; cd #{DUMMY_PATH};
    echo "gem 'adminos', git: 'git@gitlab.studio.molinos.ru:adminos.git'" >> Gemfile;
    bundle; rake db:create; rails g adminos:install)
end

unless File.directory? DUMMY_LOCALE_PATH
  system %Q(rails _3.2.13_ new #{DUMMY_LOCALE_PATH} -d postgresql; cd #{DUMMY_LOCALE_PATH};
    echo "gem 'adminos', git: 'git@gitlab.studio.molinos.ru:adminos.git'" >> Gemfile;
    bundle; rake db:create; rails g adminos:install --locales=en,nl)
end

ENV['RAILS_ENV'] = 'test'
