require 'test_helper'

class AdminosLocaleGeneratorTest < Rails::Generators::TestCase
  destination DUMMY_LOCALE_PATH

  test 'Assert locale generation' do
    name = 'event_locale'

    unless File.exist? "#{DUMMY_LOCALE_PATH}/app/models/#{name}.rb"
      system %Q(cd #{DUMMY_LOCALE_PATH}; rails g adminos #{name.classify} --locales)
    end

    assert_file "app/views/admin/#{name.pluralize}/_locale_fields.haml"
    assert_file "app/views/admin/#{name.pluralize}/_general_fields.haml"

    assert_file "app/models/#{name}.rb"
    assert_file 'config/routes.rb', /resources :#{name.pluralize}, except: :show/
    assert_file 'app/views/shared/admin/_navbar.haml', /= top_menu_item active: 'admin\/#{name.pluralize}\#'/
    assert_file 'config/locales/ru.yml', /#{name.pluralize}:/
    assert_file "app/controllers/admin/#{name.pluralize}_controller.rb"
    assert_directory "app/views/admin/#{name.pluralize}"
  end
end
