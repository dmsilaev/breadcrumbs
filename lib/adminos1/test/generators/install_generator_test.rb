require 'test_helper'

class InstallGeneratorTest < Rails::Generators::TestCase
  destination DUMMY_PATH

  test 'Assert models' do
    assert_file 'app/models/user.rb'
    assert_file 'app/models/page.rb'
    assert_file 'app/models/ability.rb'
    assert_file 'app/models/settings.rb'
    assert_file 'app/models/authentication.rb'
    assert_file 'app/models/inline_image.rb'
    assert_file 'app/models/inline_attachment.rb'
  end

  test 'Assert controllers' do
    assert_file 'app/controllers/pages_controller.rb'
    assert_file 'app/controllers/application_controller.rb'
    assert_file 'app/controllers/authentications_controller.rb'
    assert_file 'app/controllers/admin/base_controller.rb'
    assert_file 'app/controllers/admin/helps_controller.rb'
    assert_file 'app/controllers/admin/inline_attachments_controller.rb'
    assert_file 'app/controllers/admin/inline_images_controller.rb'
    assert_file 'app/controllers/admin/pages_controller.rb'
    assert_file 'app/controllers/admin/server_controller.rb'
    assert_file 'app/controllers/admin/settings_controller.rb'
    assert_file 'app/controllers/admin/users_controller.rb'
    assert_file 'app/controllers/admin/versions_controller.rb'
  end

  test 'Assert views' do
    assert_directory 'app/views/admin'
    assert_directory 'app/views/admin/base'
    assert_directory 'app/views/admin/helps'
    assert_directory 'app/views/admin/inline_attachments'
    assert_directory 'app/views/admin/inline_images'
    assert_directory 'app/views/admin/pages'
    assert_directory 'app/views/admin/settings'
    assert_directory 'app/views/admin/versions'
    assert_directory 'app/views/admin/users'
    assert_directory 'app/views/authentications'
    assert_directory 'app/views/devise'
    assert_directory 'app/views/kaminari'
    assert_directory 'app/views/layouts'
    assert_directory 'app/views/shared'
    assert_file 'app/views/pages/show.haml'
  end

  test 'Assert config' do
    assert_file 'config/initializers/simple_form.rb'
    assert_file 'config/initializers/autoprefixer.yml'
    assert_file 'config/initializers/kaminari_config.rb'
    assert_file 'config/locales/devise.ru.yml'
    assert_file 'config/locales/ru.yml'
    assert_file 'config/routes.rb'
    assert_file 'config/application.rb', /config.time_zone = 'Moscow'/
    assert_file 'config/application.rb', /config.i18n.default_locale = :ru/
  end

  test 'Assert mailers' do
    assert_file 'app/mailers/notifier.rb'
  end

  test 'Assert uploaders' do
    assert_file 'app/uploaders/common_uploader.rb'
    assert_file 'app/uploaders/inline_image_uploader.rb'
    assert_file 'app/uploaders/inline_attachment_uploader.rb'
  end

  test 'Assert assets' do
    assert_file 'app/assets/stylesheets/admin/application.scss'
    assert_file 'app/assets/javascripts/admin/application.js'
  end

  test 'Assert deploy' do
    assert_file 'config/deploy.rb'
    assert_file 'config/deploy_tasks/application.yml.erb'
    assert_file 'config/deploy_tasks/application_config.rb'
    assert_file 'config/deploy_tasks/delayed_job.rb'
    assert_file 'config/deploy_tasks/god.rb'
    assert_file 'config/deploy_tasks/god.rb.erb'
    assert_file 'config/deploy_tasks/setup.rb'
    assert_file 'config/deploy_tasks/symlink.rb'
    assert_file 'config/deploy_tasks/unicorn.rb'
    assert_file 'config/deploy_tasks/unicorn.rb.erb'
  end

  test 'Assert general' do
    assert_file 'public/404.html'
    assert_file 'public/500.html'
    assert_file 'Capfile'
    assert_file 'Gemfile'
  end
end
