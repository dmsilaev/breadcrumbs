class PopulateUsers < ActiveRecord::Migration
  def self.up
    User.create email: 'studio@molinos.ru', password: 'molinosru', roles: [:admin]
  end

  def self.down
    User.find_by_email('studio@molinos.ru').delete
  end
end
