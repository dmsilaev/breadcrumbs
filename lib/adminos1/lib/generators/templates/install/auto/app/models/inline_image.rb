class InlineImage < ActiveRecord::Base
  mount_uploader :image, InlineImageUploader

  validates :image, presence: true

  def parse_input preview=nil
    image_url = preview ? image.url(:preview) : image.url
    %Q[<img src="#{image_url}" />]
  end
end
