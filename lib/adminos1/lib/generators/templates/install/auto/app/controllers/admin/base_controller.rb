class Admin::BaseController < ApplicationController
  include Adminos::Controllers::AdminExtension
  layout 'admin/base'

  before_action :authorize, :define_breadcrumb_struct

  private

  def authorize
    authorize! :manage, :all
  end

  def define_breadcrumb_struct
    Struct.new('Breadcrumb', :label, :url) unless defined?(Struct::Breadcrumb)
  end
end
