class Admin::InlineAttachmentsController < Admin::BaseController
  skip_before_action :verify_authenticity_token, only: :create

  def create
    attachment = InlineAttachment.create file: params[:upload]

    content_type = attachment.file.file.content_type
    css_class = get_css_class(content_type)

    url = attachment.file.url
    width, height = (params[:width] || 320), (params[:height] || 240)

    @insert = insert_link width: width, height: height, content_type: content_type,
                          url: url, attachment: attachment, css_class: css_class

    render layout: false
  end

  private

  def get_css_class(content_type) # rubocop:disable CyclomaticComplexity
    case content_type
    when app_types('msword', 'vnd.openxmlformats-officedocument.wordprocessingml.document') then :doc
    when app_types('vnd.ms-excel', 'vnd.openxmlformats-officedocument.spreadsheetml.sheet') then :xls
    when app_types('zip', 'x-7z-compressed', 'x-tar', 'gzip', 'x-bzip2', 'rar', 'x-rar-compressed') then :rar
    when app_types('pdf') then :pdf
    when app_types('vnd.ms-powerpoint', 'vnd.openxmlformats-officedocument.presentationml.presentation') then :ppt
    when app_types('x-shockwave-flash') then :swf
    when img_types('gif', 'jpeg', 'pjpeg', 'x-png', 'png') then :img
    end
  end

  def app_types(*types)
    /application\/(#{types.join(' || ')})/
  end

  def img_types(*types)
    /image\/(#{types.join(' || ')})/
  end

  def insert_link(args = {})
    case args[:css_class]
    when :swf
      %(<object width="#{args[:width]}" height="#{args[:height]}"
                type="#{args[:content_type]}" data="#{args[:url]}">
        <param name="src" value="#{args[:url]}"></object>
      )
    else
      %(<a href="#{args[:url]}">#{args[:attachment].file.file.filename}</a>)
    end
  end
end
