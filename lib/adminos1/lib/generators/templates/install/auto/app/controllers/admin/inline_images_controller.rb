class Admin::InlineImagesController < Admin::BaseController
  skip_before_action :verify_authenticity_token, only: [:add_item, :destroy]
  before_action :gallery, only: [:new, :add_item]
  layout :layout

  def create
    @insert = params[:html_insert]
  end

  def add_item
    @image = InlineImage.create({gallery: @gallery}.merge params[:inline_image])
  end

  def resource
    InlineImage.new
  end
  helper_method :resource

  def destroy
    InlineImage.find(params[:id]).destroy
    head :ok
  end

  private

  def layout
    'admin/gallery_images' if action_name == 'new'
  end

  def gallery
    @gallery = true if params[:gallery] == 'true' or params[:gallery] == '1'
    @gallery ||= false
  end
end
