module ShortcutMacros
  def create type, *args
    FactoryGirl.create type, *args
  end

  def build type, *args
    FactoryGirl.build type, *args
  end
end
