set(:unicorn_config) { "#{current_path}/config/unicorn.rb" }
set(:unicorn_pid)    { "#{pids_dir}/unicorn.pid" }
set :unicorn_binary, :unicorn_rails

before "#{application}:symlink", "#{application}:unicorn_config"

namespace :"#{application}" do
  desc 'Deploy Unicorn config file to shared path on server'
  task :unicorn_config, roles: :app do
    data = ERB.new(IO.read(File.dirname(__FILE__) + "/unicorn.rb.erb")).result(binding)
    put data, "#{shared_path}/config/unicorn.rb"
  end
end

namespace :unicorn do
  task :start, roles: :app, except: { no_release: true } do
    run <<-EOF
      cd #{current_path} && RAILS_ENV=#{rails_env} \
        bundle exec god --port=#{god_port} \
                        --pid=#{god_pid} \
                        start #{god_application_identifier}_unicorn
    EOF
  end

  task :stop, roles: :app, except: { no_release: true } do
    run <<-EOF
      cd #{current_path} && RAILS_ENV=#{rails_env} \
        bundle exec god --port=#{god_port} \
                        --pid=#{god_pid} \
                        stop #{god_application_identifier}_unicorn
    EOF
  end

  task :restart, roles: :app, except: { no_release: true } do
    run <<-EOF
      cd #{current_path} && RAILS_ENV=#{rails_env} \
        bundle exec god --port=#{god_port} \
                        --pid=#{god_pid} \
                        restart #{god_application_identifier}_unicorn
    EOF
  end
end
