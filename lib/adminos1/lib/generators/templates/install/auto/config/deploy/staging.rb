server "#{application}.studio.molinos.ru", :app, :web, :db, primary: true
set :user, :ror
set :user_home_dir, "/home/#{user}"
set :god_port, PLACEHOLDER
set :keep_releases, 3
set :rails_env, 'staging'
set :unicorn_env, 'staging'
set :unicorn_worker_processes, 1
set :deploy_to, "#{user_home_dir}/hosts/#{application}"
