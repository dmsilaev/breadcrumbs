  devise_for :users, controllers: { omniauth_callbacks: 'authentications' }
  devise_scope :user do
    get 'authentications/new', to: 'authentications#new'
    post 'authentications/link', to: 'authentications#link'
  end

