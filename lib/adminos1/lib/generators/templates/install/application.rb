    config.generators{|g| g.test_framework :rspec }

    config.to_prepare do
      Devise::SessionsController.layout 'admin'
      Devise::RegistrationsController.layout proc{ |controller| user_signed_in? ? 'application' : 'admin' }
      Devise::ConfirmationsController.layout 'admin'
      Devise::UnlocksController.layout 'admin'
      Devise::PasswordsController.layout 'admin'
    end

    config.action_mailer.default_url_options = { host: 'molinos.ru' }
