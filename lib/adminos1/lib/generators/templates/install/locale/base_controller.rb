
  # Needed to properly submit params with locale different from default
  # Remove blank globalize translations based on required NAME field
  def cleanup_globalize
    translation_params = params[resource_params][:translations_attributes] || {} # параметры локали, которые пришли
    required_attrs = resource_class.required_translated_attributes # обязательные аттрибуты модели для локали

    to_translate = presence_required_translation_attrs?(translation_params, required_attrs)

    # Если обязательные параметры есть, то продолжаем сохранение в дефолтной локали для сохранения параметров
    # если нет, то сохраняем только русскую версию
    return Globalize.with_locale(I18n.default_locale) { yield } if to_translate
    params[resource_params][:translations_attributes] = {}
    yield
  end

  private

  def presence_required_translation_attrs?(translation_params, required_attrs)
    # Выбираем параметры в которых нет пустых значений по обязательным аттрибутам модели
    # Возвращаем признак наличия.
    translation_params.select do |_id, attrs|
      !attrs_has_empty_required?(attrs, required_attrs)
    end.present?
  end

  def attrs_has_empty_required?(attrs, required_attrs)
    # Проверяем в пришедшем attrs наличие пустого значения по обязательным аттрибутам
    attrs.values_at(*required_attrs).select(&:empty?).present?
  end