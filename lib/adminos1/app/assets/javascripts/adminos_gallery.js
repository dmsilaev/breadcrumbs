//= require jquery-fileupload/vendor/jquery.ui.widget
//= require jquery-fileupload/jquery.iframe-transport
//= require jquery-fileupload/jquery.fileupload

$(document).ready(function(){
  // Fix rails feature with adding '[]' in the end
  $('#inline_image_image').attr('name', 'inline_image[image]');

  $('#inline_image_image').fileupload({
    url: '/admin/inline_images/add_item',
    dataType: 'script',
    formData: {gallery: $('#gallery').is(':checked')},
    beforeSend: function(){
      $('#progress').show();
    },
    progressall: function(e, data){
      var progress = parseInt(data.loaded / data.total * 100, 10);
      if (progress==100) { $('#progress').hide() };
      $('#progress .bar').css(
          'width',
          progress + '%'
      );
    }
  });

  $('body').on('click', 'form .delete', function(e){
    e.preventDefault();
    var link = $(this);

    $.ajax({
      url: link.parents('form').attr('action'),
      type: 'delete',
      success: function(){
        link.parents('.image').remove();
      }
    });
  });

  $('body').on('submit', 'form', function(){
    $('#images .image').each(function(i, el){
      $('#html_insert').val($('#html_insert').val() + $(el).data('html-insert'));
    });
  });
});
