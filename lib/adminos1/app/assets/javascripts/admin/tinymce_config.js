$(function() {
  $('textarea.mce').tinymce({
    width : "720",
    height : "520",
    plugins : "autolink lists pagebreak layer table preview searchreplace print contextmenu paste directionality fullscreen noneditable visualchars nonbreaking iupload fupload gallery media code",
    toolbar : "undo redo formatselect alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | gallery iupload fupload media | code",
    content_css : "/assets/admin/wysiwyg.css",
    relative_urls : false,
    language : "ru",
    valid_elements : "@[lang|title|class|style|id|data-speed|data-delay"
      +"|data-src|onclick|data-screenwidth|data-alt]"
      +",strong/b,blockquote,br,caption,dd,dl,dt,em,h1,h2,h3,h4"
      +",h5,h6,hr,i,li,p,s,small,span,strikesub,sup,table,tbody,tfoot,thead"
      +",tr,u,ul,div,section,article,noscript"
      +",a[href|target|name],img[alt|height|src|width|usemap]"
      +",ol[start],td[colspan|rowspan],th[colspan|rowspan],param[name|value]"
      +",iframe[title|type|width|height|src|frameborder|allowFullScreen],map[name]"
      +",object[data|type|width|height],area[shape|href|class|coords|alt]",
    extended_valid_elements : "span[class]" //to prevent empty span removal <http://forums.modx.com/thread/45718/solved-tinymce-is-removing-divs-from-the-code?page=2#dis-post-421063>
  });

  $('textarea.mce_brief, textarea.mce_newsletter').tinymce({
    width : "720",
    height : "170",
    plugins : "autolink lists pagebreak layer table preview searchreplace print contextmenu paste directionality fullscreen noneditable visualchars nonbreaking code",
    toolbar : "undo redo formatselect alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | code",
    content_css : "/assets/admin/wysiwyg.css",
    relative_urls : false,
    language : "ru"
  });
})
