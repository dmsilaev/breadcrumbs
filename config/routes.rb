Breadcrumbs::Application.routes.draw do
  devise_for :users, skip: :all
  namespace :admin do
    resources :breads, except: :show do
      collection { post :batch_action }
    end

    resources :helps, only: :index
    resource  :settings, only: [:edit, :update]

    resources :users, except: :show do
      collection { post :batch_action }
    end

    resources :versions, only: [:index, :show] do
      collection { post :batch_action }
    end

    resources :inline_attachments, only: :create
    resources :inline_images, only: [:new, :create, :destroy] do
      post 'add_item', on: :collection
    end

    resources :pages, except: :show do
      collection { post :batch_action }
      member { put :drop }
    end

    root to: 'pages#index'
  end

  devise_for :users, controllers: { omniauth_callbacks: 'authentications' }
  devise_scope :user do
    get 'authentications/new', to: 'authentications#new'
    post 'authentications/link', to: 'authentications#link'
  end

  root to: 'index#index'

  begin
    Page.for_routes.group_by(&:behavior).each do |behavior, pages|
      pages.each do |page|
        case behavior
        when nil
        else
          resource( "#{page.class.name.underscore}_#{page.id}",
                    path:       page.absolute_path,
                    controller: behavior,
                    only:       :show,
                    page_id:    page.id )
        end
      end
    end
  rescue
    nil
  end
end
