require 'bundler/capistrano'
require 'rvm/capistrano'
require 'capistrano/ext/multistage'
require 'capistrano-db-tasks'
# require 'whenever/capistrano'
# require 'airbrake/capistrano'

set :application, :breadcrumbs
set :branch, :master
set :god_shell, '/bin/bash'
set :repository, "git@gitlab.studio.molinos.ru:#{application}.git"
set :scm, :git
set :sync_rsync_cmd, 'rsync -rv --stats --delete --compress' #cap_sync: for compatibility with mac osx
set :use_sudo, false
set :deploy_via, :remote_cache
set :normalize_asset_timestamps, false

ssh_options[:forward_agent] = true
ssh_options[:paranoid] = false

set :rvm_type, :user
set(:rvm_ruby_string) { 'ruby-2.0.0' }

set :db_local_clean, true
set :assets_dir, %w(public/system)

# set :whenever_command, 'bundle exec whenever'
# set :whenever_identifier, defer { "#{application}_#{stage}" }

set :stages, %w(staging production)
set :default_stage, 'staging'

set(:logs_dir)   { "#{current_path}/log" }
set(:pids_dir)   { "#{current_path}/tmp/pids" }
set(:config_dir) { "#{current_path}/config" }

load 'config/deploy_tasks/application_config'
load 'config/deploy_tasks/delayed_job'
load 'config/deploy_tasks/symlink'
load 'config/deploy_tasks/unicorn'
load 'config/deploy_tasks/setup'
load 'config/deploy_tasks/god'
load 'deploy/assets'

after 'deploy:restart', 'deploy:cleanup'
