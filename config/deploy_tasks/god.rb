set(:god_pid) { "#{pids_dir}/god.pid" }
set(:god_application_identifier) { "#{application}_#{stage}" }

before "#{application}:symlink", 'god:deploy_config'
before 'deploy:start', 'god:start'
after 'deploy:stop', 'god:stop'
before 'deploy:restart', 'god:restart'

namespace :god do
  task :start, roles: :app do
    run <<-EOF
      cd #{current_path} && bundle exec god -p #{god_port} \
        --pid=#{god_pid} -c #{current_path}/config/god.rb
    EOF
  end

  task :stop, roles: :app do
    begin
      run <<-EOF
        cd #{current_path} && bundle exec god -p #{god_port} \
          --pid=#{god_pid} terminate
      EOF
    rescue
      nil
    end
  end

  task :restart, roles: :app do
    run <<-EOF
      cd #{current_path} && bundle exec god --port=#{god_port} \
          --pid=#{god_pid} restart watches
    EOF
  end

  task :status, roles: :app do
    run <<-EOF
      cd #{current_path} && bundle exec god --port=#{god_port} \
        --pid=#{god_pid} status
    EOF
  end

  task :deploy_config, roles: :app do
    data = ERB.new(IO.read(File.dirname(__FILE__) + "/god.rb.erb")).result(binding)
    put data, "#{shared_path}/config/god.rb"
  end

  task :load_config, roles: :app do
    run <<-EOF
      cd #{current_path} && bundle exec god --port=#{god_port} \
        --pid=#{god_pid} load #{current_path}/config/god.rb
    EOF
  end

  task :rebuild, roles: :app do
    god.stop
    god.deploy_config
    god.start
  end
end
