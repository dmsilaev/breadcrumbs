after "deploy:update_code", "#{application}:symlink"

namespace :"#{application}" do
  desc "Make symlink for additional #{application} files"
  task :symlink do
    run "ln -nfs #{shared_path}/config/database.yml    #{release_path}/config/database.yml"
    run "ln -nfs #{shared_path}/config/application.yml #{release_path}/config/application.yml"
    run "ln -nfs #{shared_path}/config/unicorn.rb      #{release_path}/config/unicorn.rb"
    run "ln -nfs #{shared_path}/config/god.rb          #{release_path}/config/god.rb"
  end
end
