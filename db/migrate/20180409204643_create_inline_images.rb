class CreateInlineImages < ActiveRecord::Migration
  def change
    create_table :inline_images do |t|
      t.string :image
      t.boolean :gallery, default: false, null: false

      t.timestamps
    end
  end
end
