class CreateInlineAttachments < ActiveRecord::Migration
  def change
    create_table :inline_attachments do |t|
      t.string :file

      t.timestamps
    end
  end
end
