class CreateBreads < ActiveRecord::Migration
  def change
    create_table :breads do |t|
      t.string   :name
      t.boolean  :published, default: true, null: false
      t.integer :weight
      t.integer :length
      t.string :image
      t.integer :price

      t.timestamps null: false
    end
  end
end
