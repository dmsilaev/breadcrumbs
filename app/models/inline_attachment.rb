class InlineAttachment < ActiveRecord::Base
  mount_uploader :file, InlineAttachmentUploader

  validates :file, presence: true
end
