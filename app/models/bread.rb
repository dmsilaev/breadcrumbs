class Bread < ActiveRecord::Base
  include Adminos::Slugged
  include Adminos::FlagAttrs
  include Adminos::Recognizable

  scope :sorted, -> { order('created_at ASC') }
end
