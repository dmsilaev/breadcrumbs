class Admin::SettingsController < Admin::BaseController
  def update
    if resource.update_attributes params[:settings]
      flash[:notice] = t 'flash.actions.update.notice'
      redirect_to action: :edit
    else
      flash[:error] = t 'flash.actions.update.alert'
      render action: :edit
    end
  end

  private

  def resource
    @resource ||= Settings.get
  end
  helper_method :resource
end
