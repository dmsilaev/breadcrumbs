class Admin::BreadsController < Admin::BaseController
  resource( Bread,
            collection_scope: [:sorted],
            location: proc { polymorphic_path([:admin, resource.class]) },
            finder: :find_by_slug! )

  private

  alias_method :collection_orig, :collection
  def collection
    @collection ||= collection_orig
      .page(params[:page]).per(settings.per_page)
      .order("#{params[:order_by]} #{params[:direction]}")
  end
end
